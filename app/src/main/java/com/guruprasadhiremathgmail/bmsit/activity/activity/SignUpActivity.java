package com.guruprasadhiremathgmail.bmsit.activity.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.guruprasadhiremathgmail.bmsit.R;
import com.guruprasadhiremathgmail.bmsit.activity.activity.Models.RegisterPojo;
import com.guruprasadhiremathgmail.bmsit.activity.activity.RetroInterfaces.SignupInterface;
import com.guruprasadhiremathgmail.bmsit.activity.activity.RetroInterfaces.TodayScheduleInterface;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.guruprasadhiremathgmail.bmsit.activity.activity.HideKeypad.hideSoftKeyboard;

public class SignUpActivity extends AppCompatActivity {


    public EditText mUsn,mMobile,mName,mPass1,mPass2;
    public ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up2);
        setupUI(findViewById(R.id.mainrel));

//
    }


    public void registerButton(View view)
    {
//

            startActivity(new Intent(this,DashboardActivity.class));

    }


    private boolean validate(EditText editText) {
        if (editText.getText().toString().trim().length() < 1) {
            editText.setError(getString(R.string.fill));
            editText.requestFocus();
            return false;
        }
        return true;
    }


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(SignUpActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }




}
