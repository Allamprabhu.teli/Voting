package com.guruprasadhiremathgmail.bmsit.activity.activity.RetroInterfaces;

import com.guruprasadhiremathgmail.bmsit.activity.activity.Models.LoginPojo;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by allam on 26/4/17.
 */

public interface LoginInterface {

    @FormUrlEncoded
    @POST("/fbs/bmsitlogin.php")
    public void Login(@Field("usn") String usn,
                      @Field("pass") String pass,
                         Callback<LoginPojo> callback);
}
