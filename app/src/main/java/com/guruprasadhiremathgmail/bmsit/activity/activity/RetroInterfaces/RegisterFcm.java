package com.guruprasadhiremathgmail.bmsit.activity.activity.RetroInterfaces;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by allam on 19/4/17.
 */

public interface RegisterFcm {

    @FormUrlEncoded
    @POST("/fbs/registertoken.php")
    public void register(@Field("fcm_token") String fcm_token,
                         Callback<String> callback);
}
